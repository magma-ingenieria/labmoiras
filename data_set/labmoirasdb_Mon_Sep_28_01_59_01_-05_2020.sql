-- MySQL dump 10.14  Distrib 5.5.65-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: labmoirasdb
-- ------------------------------------------------------
-- Server version	5.5.65-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `casospositivos`
--

DROP TABLE IF EXISTS `casospositivos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `casospositivos` (
  `dia` varchar(255) DEFAULT NULL,
  `registros` int(11) DEFAULT NULL,
  `casos` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `casospositivos`
--

LOCK TABLES `casospositivos` WRITE;
/*!40000 ALTER TABLE `casospositivos` DISABLE KEYS */;
INSERT INTO `casospositivos` VALUES ('2020-03-18',1,1),('2020-03-20',1,2),('2020-03-21',1,3),('2020-03-22',1,4),('2020-03-24',1,5),('2020-03-25',4,9),('2020-03-26',3,12),('2020-03-27',3,15),('2020-03-28',1,16),('2020-03-29',1,17),('2020-03-30',2,19),('2020-03-31',3,22),('2020-04-02',1,23),('2020-04-03',8,31),('2020-04-04',6,37),('2020-04-05',3,40),('2020-04-06',2,42),('2020-04-07',7,49),('2020-04-08',9,58),('2020-04-09',7,65),('2020-04-10',1,66),('2020-04-11',2,68),('2020-04-12',6,74),('2020-04-13',2,76),('2020-04-14',13,89),('2020-04-15',3,92),('2020-04-16',9,101),('2020-04-17',12,113),('2020-04-18',8,121),('2020-04-19',2,123),('2020-04-20',4,127),('2020-04-21',6,133),('2020-04-22',3,136),('2020-04-23',3,139),('2020-04-24',4,143),('2020-04-25',16,159),('2020-04-26',10,169),('2020-04-27',11,180),('2020-04-28',3,183),('2020-04-29',5,188),('2020-04-30',10,198),('2020-05-02',2,200),('2020-05-03',2,202),('2020-05-04',4,206),('2020-05-05',3,209),('2020-05-06',8,217),('2020-05-07',7,224),('2020-05-08',7,231),('2020-05-09',8,239),('2020-05-10',1,240),('2020-05-11',12,252),('2020-05-12',2,254),('2020-05-13',5,259),('2020-05-14',9,268),('2020-05-15',5,273),('2020-05-16',13,286),('2020-05-17',1,287),('2020-05-18',7,294),('2020-05-19',14,308),('2020-05-20',12,320),('2020-05-21',9,329),('2020-05-22',7,336),('2020-05-23',9,345),('2020-05-24',6,351),('2020-05-25',19,370),('2020-05-26',15,385),('2020-05-27',17,402),('2020-05-28',6,408),('2020-05-29',13,421),('2020-05-30',25,446),('2020-05-31',6,452),('2020-06-01',6,458),('2020-06-02',6,464),('2020-06-03',9,473),('2020-06-04',8,481),('2020-06-05',10,491),('2020-06-06',10,501),('2020-06-07',3,504),('2020-06-08',22,526),('2020-06-09',24,550),('2020-06-10',21,571),('2020-06-11',21,592),('2020-06-12',17,609),('2020-06-13',19,628),('2020-06-14',18,646),('2020-06-15',25,671),('2020-06-16',43,714),('2020-06-17',45,759),('2020-06-18',52,811),('2020-06-19',56,867),('2020-06-20',65,932),('2020-06-21',38,970),('2020-06-22',47,1017),('2020-06-23',113,1130),('2020-06-24',105,1235),('2020-06-25',76,1311),('2020-06-26',111,1422),('2020-06-27',89,1511),('2020-06-28',74,1585),('2020-06-29',108,1693),('2020-06-30',132,1825),('2020-07-01',124,1949),('2020-07-02',164,2113),('2020-07-03',107,2220),('2020-07-04',133,2353),('2020-07-05',91,2444),('2020-07-06',78,2522),('2020-07-07',127,2649),('2020-07-08',101,2750),('2020-07-09',145,2895),('2020-07-10',147,3042),('2020-07-11',112,3154),('2020-07-12',123,3277),('2020-07-13',84,3361),('2020-07-14',140,3501),('2020-07-15',120,3621),('2020-07-16',111,3732),('2020-07-17',187,3919),('2020-07-18',140,4059),('2020-07-19',131,4190),('2020-07-20',69,4259),('2020-07-21',133,4392),('2020-07-22',196,4588),('2020-07-23',159,4747),('2020-07-24',189,4936),('2020-07-25',187,5123),('2020-07-26',114,5237),('2020-07-27',165,5402),('2020-07-28',161,5563),('2020-07-29',164,5727),('2020-07-30',151,5878),('2020-07-31',150,6028),('2020-08-01',225,6253),('2020-08-02',70,6323),('2020-08-03',199,6522),('2020-08-04',140,6662),('2020-08-05',179,6841),('2020-08-06',166,7007),('2020-08-07',103,7110),('2020-08-08',108,7218),('2020-08-09',36,7254),('2020-08-10',172,7426),('2020-08-11',93,7519),('2020-08-12',84,7603),('2020-08-13',130,7733),('2020-08-14',74,7807),('2020-08-15',85,7892),('2020-08-16',87,7979),('2020-08-17',88,8067),('2020-08-18',88,8155),('2020-08-19',78,8233),('2020-08-20',52,8285),('2020-08-21',81,8366),('2020-08-22',94,8460),('2020-08-23',44,8504),('2020-08-24',86,8590),('2020-08-25',57,8647),('2020-08-26',84,8731),('2020-08-27',76,8807),('2020-08-28',67,8874),('2020-08-29',66,8940),('2020-08-30',34,8974),('2020-08-31',97,9071),('2020-09-01',44,9115),('2020-09-02',62,9177),('2020-09-03',53,9230),('2020-09-04',80,9310),('2020-09-05',75,9385),('2020-09-06',35,9420),('2020-09-07',62,9482),('2020-09-08',60,9542),('2020-09-09',26,9568),('2020-09-10',46,9614),('2020-09-11',48,9662),('2020-09-12',62,9724),('2020-09-13',29,9753),('2020-09-14',35,9788),('2020-09-15',34,9822),('2020-09-16',19,9841),('2020-09-17',27,9868),('2020-09-18',33,9901),('2020-09-19',14,9915),('2020-09-20',1,9916),('2020-09-21',12,9928),('2020-09-22',7,9935),('2020-09-23',6,9941),('2020-09-24',1,9942),('2020-09-25',4,9946);
/*!40000 ALTER TABLE `casospositivos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fallecidos`
--

DROP TABLE IF EXISTS `fallecidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fallecidos` (
  `dia` varchar(255) DEFAULT NULL,
  `registros` int(11) DEFAULT NULL,
  `casos` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fallecidos`
--

LOCK TABLES `fallecidos` WRITE;
/*!40000 ALTER TABLE `fallecidos` DISABLE KEYS */;
INSERT INTO `fallecidos` VALUES ('2020-03-23',1,1),('2020-03-28',1,2),('2020-04-05',1,3),('2020-04-07',1,4),('2020-04-08',1,5),('2020-04-09',1,6),('2020-04-12',2,8),('2020-04-18',2,10),('2020-04-22',1,11),('2020-04-23',1,12),('2020-04-26',2,14),('2020-04-30',1,15),('2020-05-02',1,16),('2020-05-04',2,18),('2020-05-05',1,19),('2020-05-07',1,20),('2020-05-09',1,21),('2020-05-23',2,23),('2020-05-29',1,24),('2020-06-02',1,25),('2020-06-10',2,27),('2020-06-11',2,29),('2020-06-12',1,30),('2020-06-14',3,33),('2020-06-15',1,34),('2020-06-16',3,37),('2020-06-17',2,39),('2020-06-18',5,44),('2020-06-19',1,45),('2020-06-20',1,46),('2020-06-21',1,47),('2020-06-22',2,49),('2020-06-23',2,51),('2020-06-24',2,53),('2020-06-25',3,56),('2020-06-26',3,59),('2020-06-27',2,61),('2020-06-28',3,64),('2020-06-29',2,66),('2020-06-30',4,70),('2020-07-01',9,79),('2020-07-02',7,86),('2020-07-03',4,90),('2020-07-04',1,91),('2020-07-05',7,98),('2020-07-06',8,106),('2020-07-07',2,108),('2020-07-08',5,113),('2020-07-09',6,119),('2020-07-10',2,121),('2020-07-11',11,132),('2020-07-12',4,136),('2020-07-13',1,137),('2020-07-14',3,140),('2020-07-15',6,146),('2020-07-16',8,154),('2020-07-17',7,161),('2020-07-18',7,168),('2020-07-19',3,171),('2020-07-20',3,174),('2020-07-21',9,183),('2020-07-22',5,188),('2020-07-23',4,192),('2020-07-24',6,198),('2020-07-25',6,204),('2020-07-26',4,208),('2020-07-27',6,214),('2020-07-28',8,222),('2020-07-29',6,228),('2020-07-30',6,234),('2020-07-31',8,242),('2020-08-01',5,247),('2020-08-02',3,250),('2020-08-03',9,259),('2020-08-04',4,263),('2020-08-05',8,271),('2020-08-06',4,275),('2020-08-07',2,277),('2020-08-08',7,284),('2020-08-09',3,287),('2020-08-10',7,294),('2020-08-11',8,302),('2020-08-12',4,306),('2020-08-13',8,314),('2020-08-14',5,319),('2020-08-15',7,326),('2020-08-16',7,333),('2020-08-17',5,338),('2020-08-18',3,341),('2020-08-19',5,346),('2020-08-20',1,347),('2020-08-21',5,352),('2020-08-22',5,357),('2020-08-23',3,360),('2020-08-24',8,368),('2020-08-25',4,372),('2020-08-26',6,378),('2020-08-27',6,384),('2020-08-28',6,390),('2020-08-29',1,391),('2020-08-30',2,393),('2020-08-31',6,399),('2020-09-01',1,400),('2020-09-02',2,402),('2020-09-03',6,408),('2020-09-04',5,413),('2020-09-05',3,416),('2020-09-06',5,421),('2020-09-07',3,424),('2020-09-08',1,425),('2020-09-10',1,426),('2020-09-11',2,428),('2020-09-12',1,429),('2020-09-14',4,433),('2020-09-15',2,435),('2020-09-16',3,438),('2020-09-17',1,439),('2020-09-18',4,443),('2020-09-19',2,445),('2020-09-20',2,447),('2020-09-21',2,449),('2020-09-22',2,451),('2020-09-23',1,452),('2020-09-26',1,453);
/*!40000 ALTER TABLE `fallecidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fechacorte`
--

DROP TABLE IF EXISTS `fechacorte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fechacorte` (
  `hora` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fechacorte`
--

LOCK TABLES `fechacorte` WRITE;
/*!40000 ALTER TABLE `fechacorte` DISABLE KEYS */;
INSERT INTO `fechacorte` VALUES ('Sun Sep 27 05:00:25 -05 2020\n');
/*!40000 ALTER TABLE `fechacorte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recuperados`
--

DROP TABLE IF EXISTS `recuperados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recuperados` (
  `dia` varchar(255) DEFAULT NULL,
  `registros` int(11) DEFAULT NULL,
  `casos` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recuperados`
--

LOCK TABLES `recuperados` WRITE;
/*!40000 ALTER TABLE `recuperados` DISABLE KEYS */;
INSERT INTO `recuperados` VALUES ('2020-04-05',1,1),('2020-04-06',1,2),('2020-04-07',1,3),('2020-04-08',2,5),('2020-04-10',1,6),('2020-04-11',1,7),('2020-04-13',1,8),('2020-04-15',1,9),('2020-04-16',3,12),('2020-04-17',4,16),('2020-04-18',5,21),('2020-04-20',1,22),('2020-04-21',1,23),('2020-04-23',4,27),('2020-04-24',1,28),('2020-04-26',1,29),('2020-04-28',1,30),('2020-04-29',1,31),('2020-04-30',4,35),('2020-05-01',3,38),('2020-05-02',11,49),('2020-05-03',1,50),('2020-05-04',4,54),('2020-05-05',5,59),('2020-05-06',4,63),('2020-05-07',5,68),('2020-05-08',2,70),('2020-05-09',4,74),('2020-05-10',2,76),('2020-05-11',6,82),('2020-05-12',1,83),('2020-05-13',5,88),('2020-05-14',8,96),('2020-05-15',2,98),('2020-05-16',7,105),('2020-05-17',4,109),('2020-05-19',1,110),('2020-05-20',5,115),('2020-05-21',2,117),('2020-05-22',3,120),('2020-05-24',3,123),('2020-05-25',9,132),('2020-05-26',5,137),('2020-05-27',1,138),('2020-05-28',9,147),('2020-05-29',3,150),('2020-05-30',2,152),('2020-05-31',37,189),('2020-06-01',3,192),('2020-06-02',14,206),('2020-06-03',4,210),('2020-06-04',4,214),('2020-06-05',15,229),('2020-06-06',8,237),('2020-06-07',7,244),('2020-06-08',7,251),('2020-06-09',4,255),('2020-06-10',11,266),('2020-06-11',6,272),('2020-06-12',8,280),('2020-06-13',6,286),('2020-06-14',3,289),('2020-06-15',4,293),('2020-06-16',8,301),('2020-06-17',14,315),('2020-06-18',2,317),('2020-06-19',10,327),('2020-06-20',8,335),('2020-06-21',10,345),('2020-06-22',13,358),('2020-06-23',13,371),('2020-06-24',14,385),('2020-06-25',9,394),('2020-06-26',10,404),('2020-06-27',10,414),('2020-06-28',14,428),('2020-06-29',11,439),('2020-06-30',20,459),('2020-07-01',19,478),('2020-07-02',11,489),('2020-07-03',7,496),('2020-07-04',10,506),('2020-07-05',5,511),('2020-07-06',17,528),('2020-07-07',21,549),('2020-07-08',21,570),('2020-07-09',19,589),('2020-07-10',19,608),('2020-07-11',22,630),('2020-07-12',16,646),('2020-07-13',31,677),('2020-07-14',29,706),('2020-07-15',33,739),('2020-07-16',68,807),('2020-07-17',41,848),('2020-07-18',56,904),('2020-07-19',103,1007),('2020-07-20',68,1075),('2020-07-21',44,1119),('2020-07-22',44,1163),('2020-07-23',96,1259),('2020-07-24',52,1311),('2020-07-25',144,1455),('2020-07-26',116,1571),('2020-07-27',95,1666),('2020-07-28',176,1842),('2020-07-29',128,1970),('2020-07-30',63,2033),('2020-07-31',106,2139),('2020-08-01',143,2282),('2020-08-02',107,2389),('2020-08-03',42,2431),('2020-08-04',77,2508),('2020-08-05',82,2590),('2020-08-06',248,2838),('2020-08-07',89,2927),('2020-08-08',113,3040),('2020-08-09',116,3156),('2020-08-10',169,3325),('2020-08-11',50,3375),('2020-08-12',86,3461),('2020-08-13',109,3570),('2020-08-14',203,3773),('2020-08-15',300,4073),('2020-08-16',235,4308),('2020-08-17',169,4477),('2020-08-18',103,4580),('2020-08-19',249,4829),('2020-08-20',223,5052),('2020-08-21',174,5226),('2020-08-22',130,5356),('2020-08-23',189,5545),('2020-08-24',154,5699),('2020-08-25',256,5955),('2020-08-26',153,6108),('2020-08-27',79,6187),('2020-08-28',149,6336),('2020-08-29',207,6543),('2020-08-30',144,6687),('2020-08-31',79,6766),('2020-09-01',150,6916),('2020-09-02',79,6995),('2020-09-03',153,7148),('2020-09-04',80,7228),('2020-09-05',97,7325),('2020-09-06',158,7483),('2020-09-07',127,7610),('2020-09-08',91,7701),('2020-09-09',62,7763),('2020-09-10',165,7928),('2020-09-11',88,8016),('2020-09-12',51,8067),('2020-09-13',39,8106),('2020-09-14',74,8180),('2020-09-15',9,8189),('2020-09-16',22,8211),('2020-09-17',16,8227),('2020-09-18',53,8280),('2020-09-19',64,8344),('2020-09-20',45,8389),('2020-09-21',82,8471),('2020-09-22',48,8519),('2020-09-23',93,8612),('2020-09-24',118,8730),('2020-09-25',117,8847),('2020-09-26',90,8937);
/*!40000 ALTER TABLE `recuperados` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-28  1:59:01
