import smtplib
import mysql.connector
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email import encoders
from decouple import config


def _enviarDatos(nombre, correo, fecha_corte):
    cuerpo = """
    <!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <title>HTML</title>
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
        </head>
        <body>
            <p>Hola {nombre},<p>
            <p>
                Adjunto un archivo comprimido en el cuál encontrará cinco documentos en formato csv: Positivos, Recuperados, Fallecidos, Edades_Genero y Tasa_reproduccion_efectiva.<br><br>
                Los datos enviados corresponden a la fecha de actualización: {fecha_corte} y son tomados de <a href="https://www.datos.gov.co/widgets/gt2j-8ykr" target="_blank" rel="noopener noreferrer">www.datos.gov.co.</a>
            </p>
            <p>
                <strong>Nota:</strong> Debido al proceso de conversión de datos, es posible que se presente algún error en los valores decimales, se recomienda verificar que el punto no esté ausente y si lo está, para corregir el problema sólo basta agregarlo.
            </p>
            <br><br>
            <em>"Contribuimos a seguir hilando el destino - LabMoiras"</em>
            <br><br><br><br>
        </body>
    </html>
    """.format(nombre=nombre, fecha_corte=fecha_corte)
    cuerpo_html = MIMEText(cuerpo, 'html')

    msg = MIMEMultipart('alternative')
    msg['Subject'] = 'LabMoiras - Datos solicitados'
    msg['From'] = 'diegorestrepoal@unimagdalena.edu.co'
    msg['To'] = '{correo}'.format(correo=correo)
    msg.add_header('Content-Type', 'text/html')

    #zf = open('/var/www/Moiras/Reporte.zip', 'rb')
    zf = open('Reporte.zip', 'rb')

    adjunto = MIMEBase("application", "octect-stream")
    adjunto.set_payload(zf.read())
    adjunto.add_header("Content-Disposition",
                       'attachment; filename="Datos_COVID19_SantaMarta.zip"')
    encoders.encode_base64(adjunto)

    msg.attach(cuerpo_html)
    msg.attach(adjunto)

    server = smtplib.SMTP('smtp.office365.com', 587)
    server.starttls()
    server.login(msg['From'], config('PASSWORD_EMAIL'))
    server.sendmail(msg['From'], msg['To'], msg.as_string())
    server.quit()


def conectarDB():
    mydb = mysql.connector.connect(
        host='localhost',
        user='andres',
        password=config('PASSWORD_DB'),
        database='labmoirasdb'
    )

    return mydb


if __name__ == '__main__':

    mydb = conectarDB()

    cur = mydb.cursor()
    cur.execute('SELECT hora FROM fechacorte')
    fecha_corte = cur.fetchall()
    fecha_corte = fecha_corte[0][0]

    cur = mydb.cursor()
    cur.execute('SELECT * FROM enviardatos')
    enviardatos = cur.fetchall()

    if enviardatos != []:

        for i in range(len(enviardatos)):
            nombre = enviardatos[i][0]
            correo = enviardatos[i][1]

            _enviarDatos(nombre, correo, fecha_corte)

            cur.execute(
                'DELETE FROM enviardatos WHERE correo="{correo}"'.format(correo=correo))
