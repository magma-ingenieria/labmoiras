#!/bin/bash

cd /var/www/Moiras

if [ -f Reporte.zip ];
then
    rm -rf Reporte.zip
fi

if [ ! -d Reporte ];
then
    mkdir Reporte/
fi

if [ -d Reporte ];
then
    mv *.csv Reporte/
    zip -r Reporte.zip Reporte/
    rm -rf Reporte/
fi

exit 0
