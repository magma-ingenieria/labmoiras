# Código extraído de: https://github.com/k-sys/covid-19/blob/master/Realtime%20R0.ipynb


import pandas as pd
import numpy as np
from scipy import stats as sps
from scipy.interpolate import interp1d
from scipy.signal import savgol_filter
import mysql.connector
import subprocess
from decouple import config


def conectarDB():
    mydb = mysql.connector.connect(
        host='localhost',
        user='andres',
        password=config('PASSWORD_DB'),
        database='labmoirasdb'
    )

    return mydb


def leerDatosPositivos(mydb):
    cur = mydb.cursor()
    cur.execute('SELECT * FROM casospositivos')
    positivos = cur.fetchall()
    list_positivos = []

    for i in range(len(positivos)):
        list_positivos.append(list(positivos[i][0:2]))

    df = pd.DataFrame(list_positivos, columns=['date', 'positive'])
    df.to_csv('SM.csv', index=False)

    df_fecha = df['date']

    return df_fecha


def get_posteriors(sr, sigma=0.15):
    GAMMA = 1/7
    R_T_MAX = 12
    r_t_range = np.linspace(0, R_T_MAX, R_T_MAX*100+1)

    lam = sr[:-1].values * np.exp(GAMMA * (r_t_range[:, None] - 1))

    likelihoods = pd.DataFrame(
        data=sps.poisson.pmf(sr[1:].values, lam),
        index=r_t_range,
        columns=sr.index[1:])

    process_matrix = sps.norm(
        loc=r_t_range, scale=sigma).pdf(r_t_range[:, None])

    process_matrix /= process_matrix.sum(axis=0)

    prior0 = np.ones_like(r_t_range)/len(r_t_range)
    prior0 /= prior0.sum()

    posteriors = pd.DataFrame(
        index=r_t_range, columns=sr.index, data={sr.index[0]: prior0})

    log_likelihood = 0.0

    for previous_day, current_day in zip(sr.index[:-1], sr.index[1:]):
        current_prior = process_matrix @ posteriors[previous_day]
        numerator = likelihoods[current_day] * current_prior
        denominator = np.sum(numerator)
        posteriors[current_day] = numerator/denominator
        log_likelihood += np.log(denominator)

    return posteriors, log_likelihood


def find_best(cdf, p):
    n = len(cdf)
    best = (0, n-1)
    for i in range(n):
        if cdf[i] > 1.0 - p:
            break
        for j in range(n-1, i+1, -1):
            if cdf[j] - cdf[i] < p:
                break
            if j - i < best[1] - best[0]:
                best = (i, j)
    return best


def highest_density_interval(pmf, p=.9):
    index = pmf.index.values
    cdf = np.cumsum(pmf.values, axis=0)
    best = np.atleast_2d(np.apply_along_axis(
        lambda c: find_best(c, p), axis=0, arr=cdf).T)
    interval = [(index[l], index[h]) for l, h in best]

    return pd.DataFrame(interval,
                        index=(pmf.columns if isinstance(
                            pmf, pd.DataFrame) else [1]),
                        columns=[f'Rt_Low_{p*100:.0f}', f'Rt_High_{p*100:.0f}'])


def guardarDB(mydb, df_fecha, result):
    cur = mydb.cursor()
    cur.execute('DELETE FROM r_efectivo')

    for i in range(len(result)):
        cur.execute('INSERT INTO r_efectivo (dia, rt, rt_low, rt_high) VALUES ("{dia}", {rt}, {rt_low}, {rt_high})'.format(
            dia=df_fecha[i], rt=result['Rt'][i], rt_low=result['Rt_Low_90'][i], rt_high=result['Rt_High_90'][i],))


def calculate_r_efectivo():
    mydb = conectarDB()

    df_fecha = leerDatosPositivos(mydb)

    casos_SM = pd.read_csv('SM.csv', usecols=['date', 'positive'], parse_dates=[
        'date'], index_col=['date'], squeeze=True).sort_index()

    posteriors, log_likelihood = get_posteriors(casos_SM, sigma=.25)

    hdis = highest_density_interval(posteriors, p=.9)
    most_likely = posteriors.idxmax().rename('Rt')
    result = pd.concat([most_likely, hdis], axis=1)

    result['Rt'] = savgol_filter(result['Rt'], 51, 3)
    result['Rt_Low_90'] = savgol_filter(result['Rt_Low_90'], 51, 3)
    result['Rt_High_90'] = savgol_filter(result['Rt_High_90'], 51, 3)

    result[result < 0] = 0

    guardarDB(mydb, df_fecha, result)

    subprocess.run(['rm', '-rf', 'SM.csv'])
