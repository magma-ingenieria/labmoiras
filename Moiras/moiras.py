from flask import Flask, render_template, request, redirect, url_for, flash
from flaskext.mysql import MySQL
import pandas as pd
from decouple import config
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from modelo_SIR import calcular_modelo_SIR

app = Flask(__name__)

mysql = MySQL()
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
app.config['MYSQL_DATABASE_USER'] = 'andres'
app.config['MYSQL_DATABASE_PASSWORD'] = config('PASSWORD_DB')
app.config['MYSQL_DATABASE_DB'] = 'labmoirasdb'
mysql.init_app(app)


def resumenDatos(tabla):
    cur = mysql.get_db().cursor()

    cur.execute(
        'SELECT dia FROM {tabla} WHERE casos=(SELECT MAX(casos) FROM {tabla})'.format(tabla=tabla))
    resumen_dia = cur.fetchall()

    cur.execute('SELECT MAX(casos) FROM {tabla}'.format(tabla=tabla))
    resumen_casos = cur.fetchall()

    cur.execute(
        'SELECT registros FROM {tabla} WHERE casos=(SELECT MAX(casos) FROM {tabla})'.format(tabla=tabla))
    resumen_registros = cur.fetchall()

    return resumen_dia, resumen_casos, resumen_registros


def _enviarMensaje(nombre, correo, mensaje):
    cuerpo = """
        <!DOCTYPE html>
        <html lang="es">
            <head>
                <meta charset="utf-8">
                <title>Mensaje</title>
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
            </head>
            <body>
                <p>De parte de: {nombre}, {correo}<p>
                <p>
                    {mensaje}
                </p>
                <br><br><br><br>
            </body>
        </html>
    """.format(nombre=nombre, correo=correo, mensaje=mensaje)

    cuerpo_html = MIMEText(cuerpo, 'html')

    msg = MIMEMultipart('alternative')
    msg['Subject'] = 'LabMoiras - Mensaje contacto'
    msg['From'] = 'diegorestrepoal@unimagdalena.edu.co'
    msg['To'] = 'diegorestrepoal@unimagdalena.edu.co'
    msg.add_header('Content-Type', 'text/html')
    msg.attach(cuerpo_html)

    server = smtplib.SMTP('smtp.office365.com', 587)
    server.starttls()
    server.login(msg['From'], config('PASSWORD_EMAIL'))
    server.sendmail(msg['From'], msg['To'], msg.as_string())
    server.quit()


@app.route('/')
def index():
    cur = mysql.get_db().cursor()
    cur.execute('SELECT hora FROM fechacorte')
    fecha_corte = cur.fetchall()

    positivos_dia, positivos_casos, positivos_registros = resumenDatos(
        'casospositivos')
    recuperados_dia, recuperados_casos, recuperados_registros = resumenDatos(
        'recuperados')
    fallecidos_dia, fallecidos_casos, fallecidos_registros = resumenDatos(
        'fallecidos')

    cur.execute('SELECT * FROM  r_efectivo')
    r_efectivo = cur.fetchall()

    cur.execute('SELECT * FROM edadesgenero')
    poredadgenero = cur.fetchall()

    cur.execute('SELECT * FROM genero')
    sologenero = cur.fetchall()

    cur.execute('SELECT * FROM casospositivos')
    positivos = cur.fetchall()

    cur.execute('SELECT * FROM recuperados')
    recuperados = cur.fetchall()

    cur.execute('SELECT * FROM fallecidos')
    fallecidos = cur.fetchall()

    return render_template('index.html',
                           fecha_corte=fecha_corte[0],
                           positivos_dia=positivos_dia[0],
                           positivos_casos=positivos_casos[0],
                           positivos_registros=positivos_registros[0],
                           recuperados_dia=recuperados_dia[0],
                           recuperados_casos=recuperados_casos[0],
                           recuperados_registros=recuperados_registros[0],
                           fallecidos_dia=fallecidos_dia[0],
                           fallecidos_casos=fallecidos_casos[0],
                           fallecidos_registros=fallecidos_registros[0],
                           r_efectivo=r_efectivo,
                           poredadgenero=poredadgenero,
                           sologenero=sologenero[0],
                           positivos=positivos,
                           recuperados=recuperados,
                           fallecidos=fallecidos
                           )


@app.route('/plot', methods=['GET', 'POST'])
def plot():
    cur = mysql.get_db().cursor()

    cur.execute('SELECT * FROM casospositivos')
    positivos = cur.fetchall()

    cur.execute('SELECT * FROM recuperados')
    recuperados = cur.fetchall()

    cur.execute('SELECT * FROM fallecidos')
    fallecidos = cur.fetchall()

    if request.method == 'POST':
        valor = request.form['categoria']

        if valor == "Ampliar PT":
            titulo = "Casos positivos totales"
            figura = "totalesCasosPositivos"
            datos = positivos
            color = "border-primary"
        elif valor == "Ampliar RT":
            titulo = "Recuperados totales"
            figura = "recuperadosTotales"
            datos = recuperados
            color = "border-success"
        elif valor == "Ampliar FT":
            titulo = "Fallecidos totales"
            figura = "fallecidosTotales"
            datos = fallecidos
            color = "border-danger"
        elif valor == "Ampliar PD":
            titulo = "Nuevos casos positivos"
            figura = "registrosCasosPositivos"
            datos = positivos
            color = "border-primary"
        elif valor == "Ampliar RD":
            titulo = "Nuevos recuperados"
            figura = "recuperadosRegistros"
            datos = recuperados
            color = "border-success"
        elif valor == "Ampliar FD":
            titulo = "Nuevos fallecidos"
            figura = "fallecidosRegistros"
            datos = fallecidos
            color = "border-danger"

        return render_template('enlarge_plot.html', titulo=titulo, figura=figura, datos=datos, color=color)

    return render_template('plot.html', positivos=positivos, recuperados=recuperados, fallecidos=fallecidos)


@app.route('/tablaPositivos')
def tablaPositivos():
    cur = mysql.get_db().cursor()
    cur.execute('SELECT * FROM  casospositivos')
    datos = cur.fetchall()

    titulo = 'Tabla de casos positivos'
    fecha = 'Fecha de notificación'
    registro = 'Nuevo caso'
    total = 'Casos totales'

    return render_template('tabla.html', datos=datos, titulo=titulo, fecha=fecha, registro=registro, total=total)


@app.route('/tablaRecuperados')
def tablaRecuperados():
    cur = mysql.get_db().cursor()
    cur.execute('SELECT * FROM  recuperados')
    datos = cur.fetchall()

    titulo = 'Tabla de recuperados'
    fecha = 'Fecha de recuperación'
    registro = 'Nuevo recuperado'
    total = 'Recuperados totales'

    return render_template('tabla.html', datos=datos, titulo=titulo, fecha=fecha, registro=registro, total=total)


@app.route('/tablaFallecidos')
def tablaFallecidos():
    cur = mysql.get_db().cursor()
    cur.execute('SELECT * FROM  fallecidos')
    datos = cur.fetchall()

    titulo = 'Tabla de fallecidos'
    fecha = 'Fecha de la muerte'
    registro = 'Nuevo fallecido'
    total = 'Fallecido totales'

    return render_template('tabla.html', datos=datos, titulo=titulo, fecha=fecha, registro=registro, total=total)


@app.route('/tablaEdadesGenero')
def tablaEdadesGenero():
    cur = mysql.get_db().cursor()
    cur.execute('SELECT * FROM edadesgenero')
    poredadgenero = cur.fetchall()

    return render_template('tablaedadgenero.html', poredadgenero=poredadgenero)


@app.route('/r_efectivo')
def r_efectivo():
    cur = mysql.get_db().cursor()
    cur.execute('SELECT * FROM  r_efectivo')
    r_efectivo = cur.fetchall()

    return render_template('r_efectivo.html', r_efectivo=r_efectivo)


@app.route('/enviarDatos', methods=['GET', 'POST'])
def enviarDatos():

    if request.method == 'POST':
        nombre = request.form['nombre']
        correo = request.form['correo']

        cur = mysql.get_db().cursor()
        cur.execute('INSERT INTO enviardatos (nombre, correo) VALUES ("{nombre}", "{correo}")'.format(
            nombre=nombre, correo=correo))

        return render_template('enviarDatos.html', nombre=nombre, correo=correo)

    return render_template('formEnviarDatos.html')


@app.route('/about')
def about():
    return render_template('about.html')


@app.route('/contacto', methods=['GET', 'POST'])
def contacto():

    if request.method == 'POST':
        nombre = request.form['nombre']
        correo = request.form['correo']
        mensaje = request.form['mensaje']

        _enviarMensaje(nombre, correo, mensaje)

        return render_template('enviarMensaje.html', nombre=nombre)

    return render_template('formContacto.html')


@app.route('/modeloSIR', methods=['GET', 'POST'])
def modeloSIR():
    if request.method == 'POST':
        N = float(request.form['N'])
        I0 = float(request.form['I0'])
        R0 = float(request.form['R0'])
        beta = float(request.form['beta'])
        gamma = float(request.form['gamma'])
        dia = int(request.form['dia'])

        t, S, I, R = calcular_modelo_SIR(N, I0, R0, beta, gamma, dia)

        return render_template('plot_ModeloSIR.html', t=t, S=S, I=I, R=R)

    return render_template('formModeloSIR.html')


if __name__ == '__main__':
    app.run()
