import numpy as np
from scipy.integrate import odeint


def deriv(y, t, N, beta, gamma):
    S, I, R = y
    dSdt = -beta * S * I / N
    dIdt = beta * S * I / N - gamma * I
    dRdt = gamma * I
    return dSdt, dIdt, dRdt


def calcular_modelo_SIR(N, I0, R0, beta, gamma, dia):
    S0 = N - I0 - R0
    t = np.linspace(0, dia, dia)
    t = list(map(lambda x: int(x), t))

    y0 = S0, I0, R0
    ret = odeint(deriv, y0, t, args=(N, beta, gamma))
    S, I, R = ret.T

    S = S/N
    I = I/N
    R = R/N


    return t, S, I, R
