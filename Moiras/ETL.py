'''
    Nota: La ruta en la que se guardan los archivos csv es /var/www/Moiras, esto es
          en el servidor, si se van a realizar pruebas con este código, se debe modificar
          según corresponda al equipo en donde se va a correr.
'''

import pandas as pd
import subprocess
import mysql.connector
from datetime import datetime as dt
from decouple import config
from calculate_Rt import calculate_r_efectivo

'''
def convertirFormato(x):
    nuevo_formato = []

    for i in range(len(x)):
        date = dt.strptime(x[i], "%d/%m/%Y %H:%M:%S")
        nuevo_formato.append(dt.strftime(date, "%Y-%m-%d %H:%M:%S"))

    return nuevo_formato
'''


def convertirFormato(x):
    return x


def descargarDatos():
    subprocess.run(['wget', '-O', 'Colombia_raw.csv',
                    'https://www.datos.gov.co/api/views/gt2j-8ykr/rows.csv?accessType=DOWNLOAD'])


def buscarSantaMarta():
    df = pd.read_csv('Colombia_raw.csv')

    df_SantaMarta = df[df['Nombre municipio'] == 'SANTA MARTA']

    subprocess.run(['rm', '-rf', 'Colombia_raw.csv'])

    return df_SantaMarta


def preprocesarSantaMarta(df_SantaMarta, tipoFecha):
    df_fecha0 = df_SantaMarta[tipoFecha]
    df_fecha0.dropna(inplace=True)
    lista_df = df_fecha0.tolist()

    df_fecha = convertirFormato(lista_df)

    list_fecha = [df_fecha[i][0:10] for i in range(len(df_fecha))]

    list_fecha = sorted(list_fecha)
    set_fecha = set(list_fecha)

    dia = []
    registros = []
    casos = []

    for i in sorted(set_fecha):
        dia.append(i)
        registros.append(list_fecha.count(i))

    for i in range(len(registros)):
        if i == 0:
            aux = registros[i]
        elif i > 0:
            aux += registros[i]

        casos.append(aux)

    return dia, registros, casos


def filtrarSexoEdades(df_SantaMarta):
    edad_sexo = pd.DataFrame(
        {"Edad": df_SantaMarta['Edad'], "Sexo": df_SantaMarta['Sexo']})
    edad_sexo.dropna(inplace=True)

    return edad_sexo


def porGenero(edad_sexo):
    sexo = edad_sexo['Sexo']
    n_sexo = sexo.count()
    nF = sexo[sexo == 'F'].count()
    nM = sexo[sexo == 'M'].count()
    porcenF = (nF/n_sexo)*100
    porcenM = (nM/n_sexo)*100

    return nF, nM, porcenF, porcenM


def porEdadesyGenero(edad_sexo):
    edad = edad_sexo['Edad']
    edades = edad
    n_edad = edad.count()

    porEdades = []

    for i in range(11):
        if i == 10:
            porEdades.append(edades[(edades >= i*10)].count())
        else:
            porEdades.append(
                edades[(edades >= i*10) & (edades <= i*10+9)].count())

    porcen = (porEdades/n_edad)*100

    edadesF = []
    edadesM = []

    for i in range(11):
        if i == 10:
            edadesF.append(
                len(edad_sexo[(edad_sexo['Edad'] >= i*10) & (edad_sexo['Sexo'] == 'F')]))
        else:
            edadesF.append(len(edad_sexo[(edad_sexo['Edad'] >= i*10) & (
                edad_sexo['Edad'] <= i*10+9) & (edad_sexo['Sexo'] == 'F')]))

    for i in range(11):
        if i == 10:
            edadesM.append(
                len(edad_sexo[(edad_sexo['Edad'] >= i*10) & (edad_sexo['Sexo'] == 'M')]))
        else:
            edadesM.append(len(edad_sexo[(edad_sexo['Edad'] >= i*10) & (
                edad_sexo['Edad'] <= i*10+9) & (edad_sexo['Sexo'] == 'M')]))

    return porEdades, porcen, edadesF, edadesM


def conectarDB():
    mydb = mysql.connector.connect(
        host='localhost',
        user='andres',
        password=config('PASSWORD_DB'),
        database='labmoirasdb'
    )

    return mydb


def guardarDB(mydb, tabla, dia, registros, casos):
    cur = mydb.cursor()
    cur.execute('DELETE FROM {tabla}'.format(tabla=tabla))

    for i in range(len(casos)):
        cur.execute('INSERT INTO {tabla} (dia, registros, casos) VALUES ("{dia}", {registros}, {casos})'.format(
            tabla=tabla, dia=dia[i], registros=registros[i], casos=casos[i]))


def guardarDBeyg(mydb, tabla, porEdades, porcen, edadesF, edadesM):
    cur = mydb.cursor()
    cur.execute('DELETE FROM {tabla}'.format(tabla=tabla))

    inter = []

    for i in range(11):
        if i == 10:
            inter.append(str(i*10) + "+")
        else:
            inter.append(str(i*10) + "-" + str(i*10+9))

        cur.execute('INSERT INTO {tabla} (rango, cant, porcen, edadesF, edadesM) VALUES ("{rango}", {cant}, {porcen}, {edadesF}, {edadesM})'.format(
            tabla=tabla, rango=inter[i], cant=porEdades[i], porcen=porcen[i], edadesF=edadesF[i], edadesM=edadesM[i]))


def guardarDBgenero(mydb, tabla, nF, nM, porcenF, porcenM):
    cur = mydb.cursor()
    cur.execute('DELETE FROM {tabla}'.format(tabla=tabla))
    cur.execute('INSERT INTO {tabla} (nF, nM, porcenF, porcenM) VALUES (%s, %s, %s, %s)'.format(
        tabla=tabla), (nF, nM, porcenF, porcenM))


def fechaCorte(mydb):
    hora = subprocess.check_output(['date'])

    cur = mydb.cursor()
    cur.execute('DELETE FROM fechacorte')
    cur.execute('INSERT INTO fechacorte (hora) VALUES ("{hora}")'.format(
        hora=hora.decode('utf-8')))


def crearReporte(mydb):
    cur = mydb.cursor()

    cur.execute('SELECT * FROM casospositivos')
    positivos = cur.fetchall()

    cur.execute('SELECT * FROM recuperados')
    recuperados = cur.fetchall()

    cur.execute('SELECT * FROM fallecidos')
    fallecidos = cur.fetchall()

    cur.execute('SELECT * FROM edadesgenero')
    poredadgenero = cur.fetchall()

    cur.execute('SELECT * FROM  r_efectivo')
    r_efectivo = cur.fetchall()

    df_positivos = pd.DataFrame(positivos)
    df_positivos.rename(columns={
                        0: 'Fecha de notificación', 1: 'Nuevo caso', 2: 'Casos totales'}, inplace=True)
    #df_positivos.to_csv(r"/var/www/Moiras/Positivos.csv", index=None)
    df_positivos.to_csv("Positivos.csv", index=None)

    df_recuperados = pd.DataFrame(recuperados)
    df_recuperados.rename(columns={0: 'Fecha de notificación',
                                   1: 'Nuevo recuperado', 2: 'Recuperados totales'}, inplace=True)
    #df_recuperados.to_csv(r"/var/www/Moiras/Recuperados.csv", index=None)
    df_recuperados.to_csv("Recuperados.csv", index=None)

    df_fallecidos = pd.DataFrame(fallecidos)
    df_fallecidos.rename(columns={0: 'Fecha de notificación',
                                  1: 'Nuevo fallecido', 2: 'Fallecidos totales'}, inplace=True)
    #df_fallecidos.to_csv(r"/var/www/Moiras/Fallecidos.csv", index=None)
    df_fallecidos.to_csv("Fallecidos.csv", index=None)

    df_edadgenero = pd.DataFrame(poredadgenero)
    df_edadgenero.rename(columns={0: 'Rango de edades (años)', 1: 'Total personas',
                                  2: 'Porcentaje de casos positivos', 3: 'Femenino', 4: 'Masculino'}, inplace=True)
    #df_edadgenero.to_csv(r"/var/www/Moiras/Edades_Genero.csv", index=None)
    df_edadgenero.to_csv("Edades_Genero.csv", index=None)

    df_r_efectivo = pd.DataFrame(r_efectivo)
    df_r_efectivo.rename(columns={0: 'Fecha', 1: 'r efectivo',
                                  2: 'r efectivo bajo', 3: 'r efectivo alto'}, inplace=True)
    #df_r_efectivo.to_csv(r"/var/www/Moiras/Tasa_reproduccion_efectiva.csv", index=None)
    df_r_efectivo.to_csv("Tasa_reproduccion_efectiva.csv", index=None)


def run():
    descargarDatos()
    df_SantaMarta = buscarSantaMarta()

    mydb = conectarDB()

    dia, registros, casos = preprocesarSantaMarta(
        df_SantaMarta, 'Fecha de notificación')
    guardarDB(mydb, 'casospositivos', dia, registros, casos)

    dia, registros, casos = preprocesarSantaMarta(
        df_SantaMarta, 'Fecha de recuperación')
    guardarDB(mydb, 'recuperados', dia, registros, casos)

    dia, registros, casos = preprocesarSantaMarta(
        df_SantaMarta, 'Fecha de muerte')
    guardarDB(mydb, 'fallecidos', dia, registros, casos)

    edad_sexo = filtrarSexoEdades(df_SantaMarta)

    nF, nM, porcenF, porcenM = porGenero(edad_sexo)
    guardarDBgenero(mydb, 'genero', int(nF), int(nM),
                    float(porcenF), float(porcenM))

    porEdades, porcen, edadesF, edadesM = porEdadesyGenero(edad_sexo)
    guardarDBeyg(mydb, 'edadesgenero', porEdades, porcen, edadesF, edadesM)

    fechaCorte(mydb)

    calculate_r_efectivo()

    crearReporte(mydb)


if __name__ == '__main__':
    run()
