# Laboratorio de datos epidemiológicos en la nube

<div align="justify">
<b>LabMoiras</b> — es un laboratorio de estimación de escenarios epidemiológicos en la nube que se basa es sistemas de múltiples partículas interactuantes. Por medio de complejos y precisos cálculos tomados de la termodinámica y la mecánica cuántica es capaz de emular la dinámica poblacional de Santa Marta y predecir situaciones de contagio. Tiene como finalidad ayudar en la toma de decisiones a los expertos y directivos de la ciudad ante diversos escenarios de infección.
</div>

<br>

[Click ir a LabMoiras](http://labmoiras.megaingenieria.com:8080)

<br>

## Integrantes
Esta iniciativa está liderada por los grupos de investigación:

- [GIEEP (Grupo de Investigación Evaluación y Ecología Pesquera)](https://scienti.minciencias.gov.co/gruplac/jsp/visualiza/visualizagr.jsp?nro=00000000002685)
- [CMT (Teoría de la Materia Condensada)](https://scienti.minciencias.gov.co/gruplac/jsp/visualiza/visualizagr.jsp?nro=00000000002653)
- [MAGMA Ingeniería (Matemática Aplicada a la Ingeniería)](https://scienti.minciencias.gov.co/gruplac/jsp/visualiza/visualizagr.jsp?nro=00000000002680)

<br>

## Antes y ahora

<div align="justify">
Hoy, en los días del COVID-19, es cuando más nos preocupamos por el destino de la humanidad, sin embargo, no es una preocupación de ahora, desde antes, en la mitología griega las Moiras eran la personificación del destino, eran tres hermanas que por medio de una rueca determinaban los aspectos de la existencia de una persona. Es así como, la más joven hilaba la vida (dependiendo del material, el recién nacido tendría una vida desdichada o prospera, es decir, hoy sería estar sano o infectado). La segunda hermana, una mujer mayor, medía el hilo (esto determinaba el tiempo de vida de la persona, ósea, tiempo de incubación del virus). Finalmente, la última hermana, una mujer anciana que cortaba el hilo (terminando con la existencia de la persona, sea mortal o incluso un dios, tasa de mortalidad). En la actualidad tenemos una gran ventaja, contamos con potentes herramientas de cómputo, que nos ayudan a resolver muchos problemas como realizar predicciones o estimaciones, las cuales ayudan al momento de realizar la toma de decisiones sobre el futuro de la población.
</div>

